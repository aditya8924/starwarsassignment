package test.com.starwarsassignment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import test.com.starwarsassignment.R;
import test.com.starwarsassignment.views.RecyclerAdapterItemclickListener;
import test.com.starwarsassignment.model.StarWarsCharacter;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.CharacterHolder> {

    public CharactersAdapter(ArrayList<StarWarsCharacter> starWarsCharacterArrayList, RecyclerAdapterItemclickListener itemclickListener) {
        this.starWarsCharacterArrayList = starWarsCharacterArrayList;
        this.itemclickListener = itemclickListener;
    }

    private ArrayList<StarWarsCharacter> starWarsCharacterArrayList;
    private RecyclerAdapterItemclickListener itemclickListener;

    @Override
    public CharacterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.charactername_single_row, parent, false);
        return new CharacterHolder(view);
    }

    @Override
    public void onBindViewHolder(CharacterHolder holder, final int position) {
        holder.characterName.setText(starWarsCharacterArrayList.get(position).getCharacterName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemclickListener.onItemClick(starWarsCharacterArrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return starWarsCharacterArrayList.size();
    }

    class CharacterHolder extends RecyclerView.ViewHolder
    {
        TextView characterName;
        CharacterHolder(View itemView) {
            super(itemView);
            characterName = itemView.findViewById(R.id.tv_character_name);
        }
    }
}
