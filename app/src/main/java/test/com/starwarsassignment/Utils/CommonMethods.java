package test.com.starwarsassignment.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;

public class CommonMethods {

    public static String changeDateFormat(String date)
    {
        try {
            SimpleDateFormat simpleDateFormat;
            String desiredFormat = "yyyy-MM-dd HH:mm:ss";
            String defaultFormat = "yyyy-MM-dd'T'HH:mm:ss";
            simpleDateFormat = new SimpleDateFormat(defaultFormat);
            Date parsedDate = simpleDateFormat.parse(date);
            simpleDateFormat = new SimpleDateFormat(desiredFormat,Locale.ENGLISH);

            return simpleDateFormat.format(parsedDate);

        }
        catch (Exception e)
        {
            Log.e("Exception :",e.getMessage());
        }
        return "";
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
