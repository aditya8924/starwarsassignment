package test.com.starwarsassignment.retrofitinterface;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import test.com.starwarsassignment.model.StarWarsCharacterList;

public interface GetCharactersDataService {

    @GET("people/")
    Call<StarWarsCharacterList> getCharactersData(@Query("page") int pageno);
}
