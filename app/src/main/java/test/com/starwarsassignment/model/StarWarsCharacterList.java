package test.com.starwarsassignment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StarWarsCharacterList {

    @SerializedName("results")
    @Expose
    private ArrayList<StarWarsCharacter> characterArrayList;

    public ArrayList<StarWarsCharacter> getCharacterArrayList() {
        return characterArrayList;
    }

    public void setCharacterArrayList(ArrayList<StarWarsCharacter> characterArrayList) {
        this.characterArrayList = characterArrayList;
    }
}
