package test.com.starwarsassignment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StarWarsCharacter implements Serializable{
    @SerializedName("name")
    private String characterName;
    @SerializedName("height")
    private String characterHeight;
    @SerializedName("mass")
    private String characterMass;
    @SerializedName("created")
    private String characterCreatedDate;

    public StarWarsCharacter(String characterName, String characterHeight, String characterMass, String characterCreatedDate) {
        this.characterName = characterName;
        this.characterHeight = characterHeight;
        this.characterMass = characterMass;
        this.characterCreatedDate = characterCreatedDate;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getCharacterHeight() {
        if(!characterHeight.equals("0"))
        {
            return String.valueOf(Double.parseDouble(characterHeight) / 100.0);
        }
        else
        {
            return characterHeight;
        }
    }

    public void setCharacterHeight(String characterHeight) {
        this.characterHeight = characterHeight;
    }

    public String getCharacterMass() {
        return characterMass;
    }

    public void setCharacterMass(String characterMass) {
        this.characterMass = characterMass;
    }

    public String getCharacterCreatedDate() {
        return characterCreatedDate;
    }

    public void setCharacterCreatedDate(String characterCreatedDate) {
        this.characterCreatedDate = characterCreatedDate;
    }
}
