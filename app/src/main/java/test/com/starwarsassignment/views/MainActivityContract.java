package test.com.starwarsassignment.views;

import java.util.ArrayList;

import test.com.starwarsassignment.model.StarWarsCharacter;

public interface MainActivityContract {

    interface Presenter
    {
        void onDestroy();
        void requestDataFromApi(int pageno);
    }
    interface MainActivityView
    {
        void showProgress();
        void hideProgress();
        void setDataInRecyclerView(ArrayList<StarWarsCharacter> characterArrayList);
        void onFailedResponse(Throwable throwable);
    }
    interface GetCharacterInteractor
    {
        interface OnFinishedListener
        {
            void onFinished(ArrayList<StarWarsCharacter> characterArrayList);
            void onFailure(Throwable throwable);
        }
        void getCharactersArrayList(OnFinishedListener finishedListener,int pageno);
    }
}
