package test.com.starwarsassignment.views;

import test.com.starwarsassignment.model.StarWarsCharacter;

public interface RecyclerAdapterItemclickListener {
    void onItemClick(StarWarsCharacter character);
}
