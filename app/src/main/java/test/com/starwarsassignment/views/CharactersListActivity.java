package test.com.starwarsassignment.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import test.com.starwarsassignment.R;
import test.com.starwarsassignment.Utils.CommonMethods;
import test.com.starwarsassignment.adapter.CharactersAdapter;
import test.com.starwarsassignment.model.StarWarsCharacter;

public class CharactersListActivity extends AppCompatActivity implements MainActivityContract.MainActivityView{

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private MainActivityContract.Presenter presenter;
    private TextView tv_refresh;
    static final String BUNDLE_OBJECT = "object";
    static final int MAX_PAGE_SIZE = 9;
    private int CURRENT_PAGE = 1;
    private ArrayList<StarWarsCharacter> mainList =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters_list);
        initializeViews();
        presenter = new MainPresenterImpl(this, new GetCharacterInteractorImpl());

        if(CommonMethods.isNetworkConnected(this)) {
            presenter.requestDataFromApi(CURRENT_PAGE);
        }
        else {
            Toast.makeText(this, getResources().getString(R.string.no_internet_message), Toast.LENGTH_SHORT).show();
            tv_refresh.setVisibility(View.VISIBLE);
        }
    }

    private void initializeViews() {

        recyclerView = (RecyclerView) findViewById(R.id.rv_characterList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        tv_refresh = (TextView) findViewById(R.id.tv_refresh_button);
        tv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CommonMethods.isNetworkConnected(CharactersListActivity.this)) {
                    presenter.requestDataFromApi(CURRENT_PAGE);
                }
                else 
                {
                    Toast.makeText(CharactersListActivity.this, getResources().getString(R.string.no_internet_message), Toast.LENGTH_SHORT).show();
                }
            }
        });

        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }

    @Override
    public void showProgress() {

        if(progressBar!=null)
        {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {

        if(progressBar!=null)
        {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void refreshAdapter()
    {
        CharactersAdapter adapter = new CharactersAdapter(mainList, itemclickListener);
        recyclerView.setAdapter(adapter);
        tv_refresh.setVisibility(View.GONE);
        hideProgress();
    }

    @Override
    public void setDataInRecyclerView(ArrayList<StarWarsCharacter> characterArrayList) {
        if(characterArrayList.size() > 0) {
            CURRENT_PAGE = CURRENT_PAGE + 1;
            mainList.addAll(characterArrayList);
            if (CURRENT_PAGE < MAX_PAGE_SIZE) {
                presenter.requestDataFromApi(CURRENT_PAGE);
            } else {

                refreshAdapter();
            }
        }
        else
        {
            if(mainList.size() > 0)
            {
                refreshAdapter();
            }
            else
            {
                Toast.makeText(this, getResources().getString(R.string.no_data_message), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFailedResponse(Throwable throwable) {

        Toast.makeText(this, getResources().getString(R.string.error_message)+""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
        tv_refresh.setVisibility(View.VISIBLE);
    }

    RecyclerAdapterItemclickListener itemclickListener = new RecyclerAdapterItemclickListener() {
        @Override
        public void onItemClick(StarWarsCharacter character) {

            Intent characterDetails = new Intent(CharactersListActivity.this,CharacterDetailsActivity.class);
            characterDetails.putExtra(BUNDLE_OBJECT,character);
            startActivity(characterDetails);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
