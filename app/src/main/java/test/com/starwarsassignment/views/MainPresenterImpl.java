package test.com.starwarsassignment.views;

import java.util.ArrayList;

import test.com.starwarsassignment.model.StarWarsCharacter;

public class MainPresenterImpl implements MainActivityContract.Presenter,MainActivityContract.GetCharacterInteractor.OnFinishedListener {

    private MainActivityContract.MainActivityView mainActivityView;
    private MainActivityContract.GetCharacterInteractor getCharacterInteractor;

    public MainPresenterImpl(MainActivityContract.MainActivityView mainActivityView, MainActivityContract.GetCharacterInteractor getCharacterInteractor) {
        this.mainActivityView = mainActivityView;
        this.getCharacterInteractor = getCharacterInteractor;
    }

    @Override
    public void onDestroy() {
        mainActivityView = null;
    }

    @Override
    public void requestDataFromApi(int pageno) {
        mainActivityView.showProgress();
        getCharacterInteractor.getCharactersArrayList(this,pageno);
    }

    @Override
    public void onFinished(ArrayList<StarWarsCharacter> characterArrayList) {
        if (mainActivityView!=null)
        {
            mainActivityView.setDataInRecyclerView(characterArrayList);
        }
    }

    @Override
    public void onFailure(Throwable throwable) {
        if(mainActivityView!=null)
        {
            mainActivityView.onFailedResponse(throwable);
            mainActivityView.hideProgress();
        }
    }
}
