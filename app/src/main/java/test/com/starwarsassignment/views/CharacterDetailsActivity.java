package test.com.starwarsassignment.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import test.com.starwarsassignment.R;
import test.com.starwarsassignment.Utils.CommonMethods;
import test.com.starwarsassignment.model.StarWarsCharacter;

public class CharacterDetailsActivity extends AppCompatActivity {

    private TextView tvCharacterName,tvCharacterHeight,tvCharacterMass,tvCreatedDate;
    static final String BUNDLE_OBJECT = "object";
    private StarWarsCharacter starWarsCharacter;
    private double height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_details_activity));
        if(getIntent().getSerializableExtra(BUNDLE_OBJECT) != null)
        {
            starWarsCharacter = (StarWarsCharacter) getIntent().getSerializableExtra(BUNDLE_OBJECT);
            initializeViews(starWarsCharacter);
        }
    }

    private void initializeViews(StarWarsCharacter starWarsCharacter) {

        tvCharacterHeight = (TextView) findViewById(R.id.tv_height_txt);
        tvCharacterName = (TextView) findViewById(R.id.tv_name_txt);
        tvCharacterMass = (TextView) findViewById(R.id.tv_mass_txt);
        tvCreatedDate = (TextView) findViewById(R.id.tv_date_txt);
        tvCharacterName.setText(starWarsCharacter.getCharacterName());
        tvCharacterHeight.setText(starWarsCharacter.getCharacterHeight());
        tvCharacterMass.setText(starWarsCharacter.getCharacterMass());
        tvCreatedDate.setText(CommonMethods.changeDateFormat(starWarsCharacter.getCharacterCreatedDate()));
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
