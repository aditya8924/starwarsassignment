package test.com.starwarsassignment.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

import test.com.starwarsassignment.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setTimer();
    }

    private void setTimer() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent splashIntent = new Intent(SplashScreenActivity.this,CharactersListActivity.class);
                startActivity(splashIntent);
                finish();
            }
        }, 2000);
    }
}
