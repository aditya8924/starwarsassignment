package test.com.starwarsassignment.views;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.com.starwarsassignment.model.StarWarsCharacterList;
import test.com.starwarsassignment.network.RetrofitInstance;
import test.com.starwarsassignment.retrofitinterface.GetCharactersDataService;

public class GetCharacterInteractorImpl implements MainActivityContract.GetCharacterInteractor {


    @Override
    public void getCharactersArrayList(final OnFinishedListener finishedListener,int pageno) {

        GetCharactersDataService service = RetrofitInstance.getRetrofitInstance().create(GetCharactersDataService.class);
        Call<StarWarsCharacterList> call = service.getCharactersData(pageno);
        call.enqueue(new Callback<StarWarsCharacterList>() {
            @Override
            public void onResponse(Call<StarWarsCharacterList> call, Response<StarWarsCharacterList> response) {
                finishedListener.onFinished(response.body().getCharacterArrayList());
            }

            @Override
            public void onFailure(Call<StarWarsCharacterList> call, Throwable t) {
                finishedListener.onFailure(t);
            }
        });
    }


}
